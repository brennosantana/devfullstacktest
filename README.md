## Quem somos n�s?

Empresa criada em 1999 com objetivo de encontrar solu��es que realmente atendessem as necessidades de TI hospitalar, agregando a isso a experi�ncia de gestores que trabalham na �rea da Sa�de. 

Em 2009 e 2012 ganhamos o pr�mio Competitividade para Micros e pequenas empresas no Segmento TI a n�vel estadual, sendo automaticamente classificado e ficando entre os finalistas do pr�mio nacional. Somos uma empresa graduada pela incubadora de empresas da Universidade Federal de Sergipe e com reconhecimento de institui��es importantes no seguimento de sa�de, inclusive em algumas institui��es acreditadas e certificadas.


**O trabalho por aqui**

- Pr�ticas �geis como Kanban / Scrum
- Git
- PHP 5.3 ou superior
- Node
- Postgres
- React
- Vue
- Laravel

## Teste FullStack

```
Dr. Magnovaldo  abriu uma cl�nica e contratou a empresa CILADA LTDA para desenvolver seu sistema. A empresa desenvolveu v�rios requisitos e durante um ano o dr usou o sistema sem problema. Sem saber o motivo, a empresa deu um calote  e levou uma parte do sistema embora, ficando apenas sua api e  o banco de dados. Agora ele busca uma nova equipe para trabalhar na cl�nica, aquele que apresentar a melhor solu��o vai ter essa oportunidade. 

Requisito 1  

    O Doutor precisa realizar novos atendimentos, sendo que cada atendimento seleciona o paciente,o(s) iten(s) e suas respectivas quantidades na tela. Antes de finalizar o atendimento, verifica a na tela item a item, sua descri��o,valores de cada um e o total antes de gerar o atendimento. 

Requisito 2
    
    Em determinados per�odos, o Dr. Magnovaldo  precisa gerar relat�rios com todos atendimentos e valores, pesquisando sempre por um per�odo .  

Requisito 3 

    Durante  o ano de 2022 v�rios  pacientes foram medicados pelo  Dr. Magnovaldo e alguns deles est�o  internados e ele � o principal suspeito da pol�cia. Na busca por sua inoc�ncia, o doutor desconfia que algum medicamento � o respons�vel.
    Todos os pacientes que foram internados tiveram evolu��es com o seguinte diagn�stico :� rea��o al�rgica grave� .  Sendo assim o doutor precisa de todos os pacientes que possuem esta  evolu��o e o(s) medicamento(s) que  tem em comum nos seus atendimentos.

Requisito 4

    Doutor precisa saber quais itens tem cadatrado no sistema.

Requisito 5 
    
    Doutor precisa de uma lista dos  5 itens  com maior consumo nos atendimentos,ordenados no maior para o menor. colunas (descri��o do item,quantidade). 

```

**Especifica��o**

- Utilizar  a api : https://comercial.medlynx.com.br/api_devtests2022_2/api/
- Desenvolver em PHP 5.3 ou superior.
- Utilizar o  padr�o MVC.
- Quando finalizar o teste, publique no github e envie um email com o t�tulo [Teste Fullstack] Finalizado para brennosantana@medlynx.com.br

**Dica** 

- Seja criativo!

**D�vida**

Se tiver qualquer d�vida sobre esse teste, envie um email com o t�tulo [Teste Fullstack] O assunto que vc deseja para brennosantana@medlynx.com.br



### Endpoints Iniciais

Antes de cada endpoint informado, o grupo "/api" deve ser inserido, EX: `https://comercial.medlynx.com.br/api_devtests2022_2/api/pessoas`


**Como realizar uma requisi��o em PHP 7** 

```
Utilizando curl.

$curl = curl_init();
curl_setopt_array($curl, [
    CURLOPT_URL => "https://comercial.medlynx.com.br/api_devtests2022_2/api/pessoas",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET"
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    echo $response;
}

```

#### Pessoas
```
[GET]/pessoas
    Retorna uma lista de pessoas com seus respectivos dados do banco.
[GET]/pessoas/{id}
    Retorna os dados da pessoa que tem o id informado.
```
#### Atendimentos
```
[GET]/atendimentos
    Retorna uma lista de atendimentos com seus respectivos dados do banco.
[GET]/atendimentos/{id}
    Retorna os dados do atendimento que tem o id informado.
[POST]/atendimentos/new
    Insere um novo atendimento no seguinte padr�o:
    {
        "id_pessoa": "1",
        "data_atendimento": "01/01/1990 00:00:00",
        "itens": [
            {"id_item": "1", "quantidade": "3"},
            {"id_item": "2", "quantidade": "2"},
            {"id_item": "3", "quantidade": "1"}
        ]
    }
```
#### Itens
```
[GET]/itens
    Retorna uma lista de itens com seus dados do banco.
[GET]/itens/{id}
    Retorna os dados do item que tem o id informado.
```

#### Lan�amentos
```
[GET]/lancamentos
    Retorna uma lista de lancamento com seus respectivos dados do banco.
[GET]/lancamentos/{id}
    Retorna os dados do lancamento que tem o id informado.
```
#### Evolu��o
```
[GET]/evolucao
    Retorna uma lista de evolu��es com seus respectivos dados do banco.
[GET]/evolucao/{id}
    Retorna os dados da evolu��o que tem o id informado.
[POST]/evolucao/new
    Insere uma nova evolu��o no seguinte padr�o:
    {
        "descricao": "Texto",
        "id_atendimento": "1",
        "data": "01/01/1990 00:00:00"
    }

```


